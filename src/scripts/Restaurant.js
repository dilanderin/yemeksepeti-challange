import restaurantData from '../data/restoranData.json';

// Constructs Restaurant
export default class Restaurant {
  /**
   * Gets Restaurant data and turns into more readable format
  */
  async getRestaurant() {
    let {
      DisplayName: name,
      AddressText: address
    } = restaurantData.d.ResultSet;

    return {name, address};
  }

  /**
   * Generates Restaurant UI 
  */
  restaurantTemplate({name, address}) {
    return `<div class="brand__name">
      ${name}
    </div>

    <div class="brand__address">
      ${address}
    </div>`;
  };
}