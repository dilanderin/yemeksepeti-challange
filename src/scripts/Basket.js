import Storage from './Storage';

let basketItems = [];
const basketTotal = document.querySelector('.js-basket-total');
const basketCount = document.querySelector('.js-basket-count');
const basketDetails = document.querySelector('.js-basket-details');
const basketContent = document.querySelector('.js-basket-content');

/**
 * Constructs Basket
*/
export default class Basket {
  /**
   * Loads all the basket when the page loads
  */
  loadBasket() {
    basketItems = Storage.getBasket();
    this.setBasketValue(basketItems);
    this.populateBasket(basketItems);
  }

  /**
   * Adds loaded basket data into the html
  */
  populateBasket(basketItems) {
    basketItems.forEach(
      item => this.addBasketItem(item)
    );
  }

  /**
   * Generates Basket UI
  */
  addBasketItem(item) {
    let container = document.createElement('div');
    container.classList.add('basket-details__content-container');

    container.innerHTML = `
    <div class="product">
      <div class="product__counter" data-id=${item.id}>
        <i class="product__counter-add js-increment-product fas fa-chevron-up"></i>

        <span class="item-amount" data-id="${item.id}">${item.amount}</span>

        <i class="product__counter-remove js-decrement-product fas fa-chevron-down"></i>
      </div>

      <div class="product__main">
        <div class="product__heading">
          <div class="product__title">
            ${item.productName}
          </div>

          <div class="product__dots"></div>
          <div class="product__price">
            ${item.productPrice}
          </div>
        </div>

        <div class="product__details">
          ${item.productDescription}
        </div>
      </div>

      <div class="product__delete js-product-remove" data-id="${item.id}">
        <i class="fas fa-trash"></i>
      </div>
    </div>`;

    basketContent.appendChild(container);
  }

  /**
   * Opens Basket details container
  */
  showBasket() {
    basketDetails.classList.add('basket-details--show');
  }

  /**
   * Closes Basket details container
  */
  hideBasket() {
    basketDetails.classList.remove('basket-details--show');
  }

  /**
   * Resets basket buttons of menu from "Added to basket" to "Add to basket"
  */
  resetBasketButton(item) {
    item.innerHTML = '<i class="fas fa-cart-plus"></i> Ekle';
    item.classList.remove('product__counter-button--added');
    item.classList.add('product__counter-button--add');
  }

  /**
   * Adds new items to basket and show it when basket button of products is clicked
  */
  getBasketButtons () {
    const buttons = document.querySelectorAll('.js-basket-button');

    buttons.forEach(item => {
      let id = item.dataset.id;
      let inBasket = basketItems.find(e => e.id === id);

      if (inBasket) {
        item.innerHTML = '';
        item.innerHTML = `${inBasket.amount} Adet`;
        item.classList.remove('product__counter-button--add');
        item.classList.add('product__counter-button--added');
      }

      item.addEventListener('click', event => {
        event.target.classList.remove('product__counter-button--add');
        event.target.classList.add('product__counter-button--added');

        let basketProduct = Storage.getProduct(id);
        let itemExists = Storage.getBasketItem(id);

        let basketItem = { ...basketProduct, amount: 1 };

        if (itemExists) {
          let newAmount = itemExists.amount + 1;
          itemExists.amount = newAmount;
          basketItem = itemExists;
        }

        basketItems = [...basketItems, basketItem];

        item.innerHTML = '';
        item.innerHTML = `${basketItem.amount} Adet`;

        Storage.storeBasket(basketItems);

        this.setBasketValue(Storage.getBasket());

        let basketItemAmount = document.querySelectorAll('.item-amount');
        basketItemAmount = basketItemAmount.forEach(item => {
          item.dataset.id == id ? item.innerText = basketItem.amount : ''
        });

        itemExists ? '' : this.addBasketItem(basketItem);

        this.showBasket();
      });
    });
  }

  /**
   * Calculates total price and amount of items for the basket
  */
  setBasketValue(basket) {
    let tempTotal = 0,
        itemsTotal = 0;

    basket.map(item => {
      tempTotal += (parseFloat((item.productPrice).replace(",", ".")) * item.amount);
      itemsTotal += item.amount;
    })

    basketTotal.innerText = parseFloat(tempTotal.toFixed(2))
    basketCount.innerText = itemsTotal;
  }

  /**
   * Removes a basket item from basket with a specific id
  */
  removeBasketItem(id) {
    basketItems = basketItems.filter(item => item.id !== id);
    this.setBasketValue(basketItems);
    Storage.storeBasket(basketItems, true);
  }
  
}