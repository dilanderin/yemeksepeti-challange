// Constructs Local Storage
export default class Storage {
  /**
   * Adds all products into Local Storage as "products"
  */
  static storeProducts(products) {
    let oldItems = JSON.parse(localStorage.getItem('products')) || [];

    // Eliminates duplicate products
    products.forEach(item => {
      if (!oldItems.find(old => old.id == item.id)) {
        oldItems.push(item);
      }
    });

    localStorage.setItem('products',
      JSON.stringify(oldItems)
    );
  }

  /**
   * Gets a product from Local Storage with a specific id
  */
  static getProduct(id) {
    let products = JSON.parse(
      localStorage.getItem('products')
    );

    return products.find(product => product.id == id)
  }

  /**
   * Adds desired products into Local Storage as "basket" items
  */
  static storeBasket(basket, reset = false, removeId) {
    let oldItems = JSON.parse(localStorage.getItem('basket')) || [];

    // Eliminates duplicate products
    // "reset" for replacing basket completely
    if (reset) {
      oldItems = basket;
    } else {
      basket.forEach(item => {
        let duplicateItem = oldItems.find(old => old.id == item.id);
        if (!duplicateItem & !removeId) {
          oldItems.push(item);
        } else {
          // if duplicate item exists, updates its amount
          duplicateItem.amount = item.amount;
        }
      });
    }

    localStorage.setItem('basket',
      JSON.stringify(oldItems)
    );
  }

  /**
   * Gets all basket items from Local Storage
  */
  static getBasket() {
    let localBasket = localStorage.getItem('basket');
    return localBasket ? JSON.parse(localBasket) : [];
  }

  /**
   * Gets a basket item from Local Storage with a specific id
  */
  static getBasketItem(id) {
    let localBasket = JSON.parse(localStorage.getItem('basket'));
    return localBasket ? localBasket.find(item => item.id == id) : '';
  }
}