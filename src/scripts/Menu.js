/**
 * Constructs Menu including products and categories
*/
export default class Menu {
  /**
   * Turns Menu Category and Products information into more readable format
  */
  getCategoryInfo(category) {
    let categoryName = category.DisplayName;
    let products = category.Products.map(
      ({
        ProductId: id,
        ListPrice: productPrice,
        DisplayName: productName,
        Description: productDescription
      }) => ({
        id,
        productName,
        productPrice,
        productDescription
      })
    );

    return {categoryName, products};
  }

  /**
   * Generates Menu UI
  */
  menuTemplate({categoryName, products}) {
    let productsTemplate = '';

    for (let {productName, productDescription, productPrice, id} of products) {
      productsTemplate +=  `
      <div class="product">
        <div class="product__counter">
          <button
            class="
              product__counter-button
              product__counter-button--add
              js-basket-button
            "
            data-id="${ id }"
          >
            <i class="fas fa-cart-plus"></i>
            Ekle
          </button>
        </div>

        <div class="product__main">
          <div class="product__heading">
            <div class="product__title">
              ${productName}
            </div>

            <div class="product__dots"></div>

            <div class="product__price">
              ${productPrice}
            </div>
          </div>

          <div class="product__details">
            ${productDescription}
          </div>
        </div>
      </div>
      `
    }

    let template = `<div class="category">
      <div class="category__title">
        ${categoryName}
      </div>

      <div class="category__products">
        ${productsTemplate}
      </div>
    </div>`;

    return template;
  }
}