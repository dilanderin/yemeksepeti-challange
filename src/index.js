import './styles/main.scss';
import menuData from './data/menuData.json';

import Storage from './scripts/Storage';
import Restaurant from './scripts/Restaurant';
import Menu from './scripts/Menu';
import Basket from './scripts/Basket';

let basketItems = [];

/**
 * Generates complete Menu UI
*/
const constructMenu = _ => {
  const menu = new Menu();

  let menuHtml = '';

  menuData.d.ResultSet.forEach(item => {
    let data = menu.getCategoryInfo(item);
    menuHtml += menu.menuTemplate(data);

    // removes category so that Local Storage only stores products
    delete data.categoryName;
    Storage.storeProducts(data.products);
  });

  let container = document.querySelectorAll(".js-menu")[0];
  container.innerHTML += menuHtml;
}

/**
 * Generates complete Restaurant UI
*/
const constructRestaurant = _ => {
  const restaurant = new Restaurant();

  restaurant.getRestaurant().then(
    result => {
      let container = document.querySelectorAll(".js-restaurant")[0];
      container.innerHTML += restaurant.restaurantTemplate(result);
    }
  );
}

/**
 * Increases or decreases of the amount of the basket items
*/
const changeCount = decrement => {
  const basket = new Basket();
  let id = event.target.closest(".product__counter").getAttribute('data-id'),
      basketItem = Storage.getBasketItem(id),
      newAmount;

  if (!decrement) {
    newAmount = basketItem.amount + 1;
  } else {
    newAmount = Math.max(1, basketItem.amount - 1);
  }

  basketItem.amount = newAmount;
  basketItems = [...basketItems, basketItem];

  for (let sibling of event.target.parentNode.children) {
    sibling.classList.contains('item-amount') ? sibling.innerText = newAmount : ''
  }

  document.querySelectorAll('.js-basket-button').forEach(item => {
    item.dataset.id == id ? item.innerText = `${basketItem.amount} Adet` : ''
  });

  Storage.storeBasket(basketItems);
  basket.setBasketValue(Storage.getBasket());
}

document.addEventListener("DOMContentLoaded", () => {
  const basket = new Basket();
  basket.loadBasket();

  constructMenu();
  constructRestaurant();

  basket.getBasketButtons();

  document.querySelector('.js-hide-basket').addEventListener('click', () => {
    basket.hideBasket();
  });

  document.querySelector('.js-basket').addEventListener('click', () => {
    basket.showBasket();
  });

  document.querySelector("body").addEventListener('click', function(event) {
    if (event.target.classList.contains('js-increment-product')) {
      changeCount();
    } else if (event.target.classList.contains('js-decrement-product')) {
      changeCount(1);
    } else if (event.target.classList.contains('js-product-remove')) {
      event.stopPropagation();
      let id = event.target.getAttribute('data-id');
      basket.removeBasketItem(id);
      event.target.closest(".product").style.display = 'none';

      let basketButton = document.querySelectorAll('.js-basket-button');
      basketButton.forEach(item => {
        if (item.dataset.id == id) {
          item.innerHTML = '';
          basket.resetBasketButton(item);
        }
      });
    }
  });

  document.querySelector('.js-basket-clear').addEventListener('click', () => {
    localStorage.removeItem('basket');
    basket.loadBasket();
    document.querySelector('.js-basket-content').innerHTML = '';
    let addedItems = document.querySelectorAll('.product__counter-button--added');

    for (const item of addedItems) {
      basket.resetBasketButton(item);
    }
  });
});

